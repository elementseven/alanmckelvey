
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('jquery');
require('./bootstrap');
require('./jquery.bxslider.js');
require('waypoints/lib/jquery.waypoints.min.js');
require('./plugins/modernizr-custom.js');
require('./a-general.js');
window.LazyLoad = require('vanilla-lazyload');
var dropin = require('braintree-web-drop-in');
window.Vue = require('vue');
window.moment = require('moment');

import VueGoogleCharts from 'vue-google-charts';
Vue.use(VueGoogleCharts);

import { VueEditor } from "vue2-editor";

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('form-loader', require('./components/Loader.vue').default);

Vue.component('loader', require('./components/admin/reuse/Loader.vue').default);
Vue.component('countries', require('./components/Countries.vue').default);

Vue.component('contact-form', require('./components/ContactPageForm.vue').default);

Vue.component('blog', require('./components/blog/Blog.vue').default);
Vue.component('blog-posts', require('./components/blog/BlogPosts.vue').default);

Vue.component('events', require('./components/events/Events.vue').default);
Vue.component('upcoming-events', require('./components/events/UpcomingEvents.vue').default);

Vue.component('website-visitors', require('./components/admin/reuse/WebsiteVisitors.vue').default);
Vue.component('user-types', require('./components/admin/reuse/UserTypes.vue').default);
Vue.component('top-referrers', require('./components/admin/reuse/TopReferrers.vue').default);
Vue.component('popular-pages', require('./components/admin/reuse/PopularPages.vue').default);

Vue.component('index-events', require('./components/admin/events/IndexEvents.vue').default);
Vue.component('create-event', require('./components/admin/events/CreateEventFrom.vue').default);
Vue.component('edit-event', require('./components/admin/events/EditEventFrom.vue').default);
Vue.component('remove-event', require('./components/admin/events/RemoveEvent.vue').default);
Vue.component('show-event', require('./components/admin/events/ShowEvent.vue').default);

Vue.component('index-locations', require('./components/admin/locations/IndexLocations.vue').default);
Vue.component('create-location', require('./components/admin/locations/CreateLocationFrom.vue').default);
Vue.component('edit-location', require('./components/admin/locations/EditLocationFrom.vue').default);
Vue.component('remove-location', require('./components/admin/locations/RemoveLocation.vue').default);
Vue.component('show-location', require('./components/admin/locations/ShowLocation.vue').default);

Vue.component('index-news', require('./components/admin/news/IndexNews.vue').default);
Vue.component('create-news', require('./components/admin/news/CreateNewsFrom.vue').default);
Vue.component('edit-news', require('./components/admin/news/EditNewsFrom.vue').default);
Vue.component('remove-news', require('./components/admin/news/RemoveNews.vue').default);
Vue.component('show-news', require('./components/admin/news/ShowNews.vue').default);

Vue.component('index-categories', require('./components/admin/categories/IndexCategories.vue').default);
Vue.component('create-category', require('./components/admin/categories/CreateCategoryFrom.vue').default);
Vue.component('remove-category', require('./components/admin/categories/RemoveCategory.vue').default);
Vue.component('show-category', require('./components/admin/categories/ShowCategory.vue').default);

Vue.component('index-products', require('./components/admin/products/IndexProducts.vue').default);
Vue.component('create-product', require('./components/admin/products/CreateProductFrom.vue').default);
Vue.component('edit-product', require('./components/admin/products/EditProductFrom.vue').default);
Vue.component('remove-product', require('./components/admin/products/RemoveProduct.vue').default);
Vue.component('show-product', require('./components/admin/products/ShowProduct.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
