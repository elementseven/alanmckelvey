var menu_counter = 0;
var scroll_top = 0;

$('#accounttrigger').click(function(){
	$(this).find('.fa-angle-right').toggleClass('fa-rotate-90');
})

// Function to resize elements to square
function squares(){
	$(".square").each(function(){
		$(this).height($(this).width() + "px");
	});
}

$(document).ready(function(){
	squares();
	var lazyLoadInstance = new LazyLoad({
	    elements_selector: ".lazy"
	});
	$('#menu_btn').click(function(){
		$("#menu_btn .nav-icon").toggleClass('nav_open');
		$("#menu").toggleClass("on");
		$("#mobile_menu").toggleClass("on");
		$("#menu_body_hide").toggleClass("on");
	});
	$('#close-loader-btn').click(function(){
		document.getElementById("loader").classList.remove("on");
	});
	var homeslider = $('#homeslider_gallery').bxSlider({
    auto: true,
    controls: false,
    autoControls: false,
    speed: 800,
    stopAutoOnClick: true,
    randomStart: false,
    pause: 5000,
    pagerCustom: $('#homeslider_modal').find('.gallery-pager'),
    onAfterSlide: function(currentSlideNumber, totalSlideQty, currentSlideHtmlObject){  
      $('#'+modalid).find('.gallery-pager a.active').removeClass('active').next().addClass('active');  
    }
  });
  $('#homeslider_modal').on('shown.bs.modal', function (e) {
  	var imgnum = e.relatedTarget;
  	imgnum = $(imgnum).attr('data-img');
	  homeslider.reloadSlider();
	  homeslider.goToSlide(imgnum);
	});
});
$(window).resize(function(){
	squares();
});