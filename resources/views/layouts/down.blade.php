<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="owsey, owsey music, Belfast, Northern Ireland, synth, synths, music, musician, cinematic, arrangements, soundscapes, sound, ambient, cool, strings, guitars, piano, vocals, indie, independant">
    <meta name="description" content="Hailing from Northern Ireland, Owen Ferguson 'Owsey' is known for his lush, cinematic production and dream-like arrangements. His soundscapes paint a vivid picture; stark, wind-swept coastines, expansive vistas and alien worlds are conjured up with haunting strings and evocative synths.">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#414141">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Belfast Motorcycle Rentals') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{ asset('css/app.css') }}?<?php echo date('l jS \of F Y h:i:s A'); ?>" media="all" rel="stylesheet">
    <style>@import url('https://fonts.googleapis.com/css?family=Abel|BenchNine:400,700');</style>
</head>
<body>
    <div id="app" class="front">
        
        <div id="content">
        @yield('content')
        

        </div>
        
        
        <div class="desby text-center" style="position:absolute;bottom:0;left:0;width:100%;">
            <p>Website by <a href="https://elementseven.co" target="_blank">Element Seven</a></p>
        </div>
    </div>
</body>
</html>