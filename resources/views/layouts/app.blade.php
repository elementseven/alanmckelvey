<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="art in motion, Art in Motion, graphics, windows, stickers, decals, vehicle, cars, buses, vans, commerical, custom, durable, one off, weather proof, artnmotion, lisburn, belfast, northern ireland, antrim, armagh, down, moira, hillsborough">
    <meta name="description" content="At Art in Motion NI, our design services cover every aspect of your image-making requirements from sign and graphic design through to production and installation.  Our services include corporate fleet, one-off custom, outdoor durable full colour graphics, signage, window graphics, banners and much, much more.">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Alan McKelvey Music') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{ asset('css/app.css') }}?<?php echo date('l jS \of F Y h:i:s A'); ?>" media="all" rel="stylesheet">
    <style>@import url('https://fonts.googleapis.com/css?family=Abel|BenchNine:400,700');</style>
    @yield('styles')
</head>
<body>
    <div id="app">
        <div id="main_menu" class="mobile_hide">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 top_menu">
                        <a href="/">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Home</div>
                            </div>
                        </a>
                        <a href="/events">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Events</div>
                            </div>
                        </a>   
                        <a href="/media">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Media</div>
                            </div>
                        </a>                    
                        <a href="/blog">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Blog</div>
                            </div>
                        </a>                    
                        <a href="/shop">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Shop</div>
                            </div>
                        </a>                        
                        <a href="/contact">
                            <div class="menu_item dis-table">
                                <div class="vert-mid">Contact</div>
                            </div>
                        </a>  
                    </div>
                    <div class="col-sm-3 text-right">  
                        <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                        <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                        <a href="https://www.youtube.com/amck" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;
                        <a href="tel:07927114818" target="_blank"><i class="fa fa-phone"></i></a> &nbsp;
                        <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="top_bar" class="mobile_hide">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 top_menu scrollFade pt-2" data-fade="slideInDown">
                        <a href="/">
                            <img src="/img/logos/big_amm_2.png" alt="Alan Mckelvey Music logo" width="150px">
                        </a>
                    </div>
                    <div class="col-sm-3 text-right white_text">
                        <div class="scrollFade" data-fade="slideInDown">
                            <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                            <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                            <a href="https://www.youtube.com/amck" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;
                            <a href="tel:07927114818" target="_blank"><i class="fa fa-phone"></i></a> &nbsp;
                            <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg1.jpg')">
                <div class="trans twoThirdsHeight">
                    <div class="dis-table twoThirdsHeight width_100">
                        <div class="vert-mid">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 white_text">
                                        <h2 class="bold">Members Area</h2>
                                        <div id="menu_trigger"></div>
                                        <p class="max_500">Download your purchases, manage your account details and view all of your orders directly from your dashboard!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @yield('content')
            <form-loader></form-loader>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="footer_logo float-left">
                            <img src="/img/logos/big_amm_2.png" alt="logo" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="mt-3">
                            <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                            <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                            <a href="https://www.youtube.com/amck" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;<br>
                            <a href="tel:07927114818" target="_blank">+4479 2711 4818</a><br>
                            <a href="mailto:info@alanmckelvey.com">info@alanmckelvey.com</a>
                        </p>
                    </div>   
                    <div class="col-md-4"> 
                        <p class="mt-3 mb-0">For booking enquiries, please get in touch using the form on the <a href="/contact" class="grey_text">contact</a> page.</p>
                        <a href="/login">
                            <div class="btn btn-white float-left mt-3">login</div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
        <div class="container-fluid desby">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p>&copy; {{Carbon\Carbon::now()->format('Y')}} Alan McKelvey - Website by <a href="http://mckelvey.digital" target="_blank">Luke McKelvey</a></p>
                    </div>
                </div>
            </div>
        </div>


        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('scripts')
    </body>
    </html>
