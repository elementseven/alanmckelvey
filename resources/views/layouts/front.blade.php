<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{$metadescription}}">
    <title>{{$pagetitle}}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{ asset('css/app.css') }}?<?php echo date('l jS \of F Y h:i:s A'); ?>" media="all" rel="stylesheet">
    <!-- Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139165135-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-139165135-1');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body class="{{$pagetype}}">
    <div id="app" class="front">
        <div id="menu_btn" class="menu_btn float-left d-lg-none"><div class="nav-icon"><span></span><span></span><span></span></div></div>
        <div id="mobile_menu" class="d-lg-none">
            <div class="mobile_menu_inner">
                <picture> 
                  <source data-srcset="/img/logos/logo-dark.webp" type="image/webp"/> 
                  <source data-srcset="/img/logos/logo-dark.png" type="image/png"/>
                  <img data-src="/img/logos/logo-dark.png" class="mb-3 lazy" width="150" alt="Alan McKelvey Music Logo dark" />
                </picture>
                <a href="/">
                    <div class="menu_item @if($pagename == "home") active @endif"><i class="fa fa-angle-right"></i> &nbsp; Home</div>
                </a>   
                <a href="/events">
                    <div class="menu_item @if($pagename == "events") active @endif"><i class="fa fa-angle-right"></i> &nbsp; Events</div>
                </a>                    
                <a href="/blog">
                    <div class="menu_item @if($pagename == "blog") active @endif"><i class="fa fa-angle-right"></i> &nbsp; Blog</div>
                </a>                    
                {{-- <a href="/shop">
                    <div class="menu_item @if($pagename == "shop") active @endif"><i class="fa fa-angle-right"></i> &nbsp; Shop</div>
                </a>      --}}                   
                <a href="/contact">
                    <div class="menu_item @if($pagename == "contact") active @endif"><i class="fa fa-angle-right"></i> &nbsp; Contact</div>
                </a>  
            </div>
            <div class="mobile_menu_social">
                <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                <a href="https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;
                <a href="tel:07927114818" target="_blank"><i class="fa fa-phone"></i></a> &nbsp;
                <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
            </div>
        </div>
        <div id="main_menu" class="mobile_hide d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 top_menu">
                        <a href="/">
                            <div class="menu_item dis-table @if($pagename == "home") active @endif">
                                <div class="vert-mid">Home</div>
                            </div>
                        </a>   
                        <a href="/events">
                            <div class="menu_item dis-table @if($pagename == "events") active @endif">
                                <div class="vert-mid">Events</div>
                            </div>
                        </a>                     
                        <a href="/blog">
                            <div class="menu_item dis-table @if($pagename == "blog") active @endif">
                                <div class="vert-mid">Blog</div>
                            </div>
                        </a>                    
                        {{-- <a href="/shop">
                            <div class="menu_item dis-table @if($pagename == "shop") active @endif">
                                <div class="vert-mid">Shop</div>
                            </div>
                        </a>      --}}                   
                        <a href="/contact">
                            <div class="menu_item dis-table @if($pagename == "contact") active @endif">
                                <div class="vert-mid">Contact</div>
                            </div>
                        </a>  
                    </div>
                    <div class="col-sm-3 text-right">  
                        <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                        <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                        <a href="https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;
                        <a href="tel:07927114818" target="_blank"><i class="fa fa-phone"></i></a> &nbsp;
                        <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="top_bar" class="d-none d-lg-block @if($pagename == 'success') dark @endif">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 top_menu scrollFade" data-fade="slideInDown">
                        <a href="/">
                            <div class="menu_item dis-table @if($pagename == "home") active @endif">
                                <div class="vert-mid">Home</div>
                            </div>
                        </a>   
                        <a href="/events">
                            <div class="menu_item dis-table @if($pagename == "events") active @endif">
                                <div class="vert-mid">Events</div>
                            </div>
                            </a>                    
                        <a href="/blog">
                            <div class="menu_item dis-table @if($pagename == "blog") active @endif">
                                <div class="vert-mid">Blog</div>
                            </div>
                        </a>                    
                        {{-- <a href="/shop">
                            <div class="menu_item dis-table @if($pagename == "shop") active @endif">
                                <div class="vert-mid">Shop</div>
                            </div>
                        </a>        --}}                 
                        <a href="/contact">
                            <div class="menu_item dis-table @if($pagename == "contact") active @endif">
                                <div class="vert-mid">Contact</div>
                            </div>
                        </a> 
                    </div>
                    <div class="col-sm-3 text-right white_text">
                        <div class="scrollFade" data-fade="slideInDown">
                            <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                            <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                            <a href="https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;
                            <a href="tel:07927114818" target="_blank"><i class="fa fa-phone"></i></a> &nbsp;
                            <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
        @yield('content')
        

        </div>
        <form-loader></form-loader>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pt-5 text-center">
                        <div class="footer_logo">
                            <picture> 
                              <source data-srcset="/img/logos/logo.webp" type="image/webp"/> 
                              <source data-srcset="/img/logos/logo.png" type="image/png"/>
                              <img data-src="/img/logos/logo.png" class="mb-3 lazy" alt="Alan McKelvey Music Logo" />
                            </picture>
                        </div>
                    </div>
                    <div class="col-md-4 pt-5 mob-pb-0 text-center">
                        <p class="social_p">
                            <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;
                            <a href="https://www.instagram.com/alanmckelvey/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;
                            <a href="https://https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play"></i></a> &nbsp;<br>
                        </p>
                        <p class="mt-3">
                            <a href="tel:07927114818" target="_blank">+44(0) 79 2711 4818</a><br>
                            <a href="mailto:info@alanmckelvey.com">info@alanmckelvey.com</a>
                        </p>
                    </div>   
                    <div class="col-md-4 py-5 mob-pt-0 text-center"> 
                        <p class="mb-0">For bookings or enquiries,<br>please get in touch.</p>
                        <a href="{{route('contact')}}">
                            <div class="btn btn-white mt-3">contact</div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
        <div class="container-fluid desby">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p>&copy; {{Carbon\Carbon::now()->format('Y')}} Alan McKelvey - Website by <a href="http://mckelvey.digital" target="_blank">Luke McKelvey</a></p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <style>@import url('https://fonts.googleapis.com/css?family=Poppins:600,800|Roboto');</style>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>