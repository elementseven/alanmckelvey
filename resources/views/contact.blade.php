@php
$page = 'Contact';
$pagetitle = 'Contact | Request a booking or simply get in touch';
$metadescription = "Do you want Alan to perform in at your venue? Do you want to know about availability or simply want to tell Alan that you enjoyed the last performance? Get in touch today and Alan will get back to you as soon as he can.";
$pagetype = 'dark';
$pagename = 'contact';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg4.jpg');">
    <div class="trans twoThirdsHeight">
        <div class="dis-table twoThirdsHeight width_100">
            <div class="vert-mid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 white_text">
                            <h1 class="page_title">Contact</h1>
                            <div id="menu_trigger"></div>
                            <p class="max_500">If you have any queries or general questions, please drop me a message using the form below.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container py-5">
    <div id="menu_trigger"></div>
    <div class="row product">
        <div class="col-12 mt-3">
            <contact-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-form>
        </div>
    </div>

</div>  

<div class="container-fluid">
  <div id="social_stripe" class="row dark_bg text-center py-5 bg">
    <div class="col-sm-12">
      <h2 class="mb-0 scrollFade" data-fade="fadeIn">Follow</h2>
      <p class="mb-3 text-white">Follow on social media or get in touch</p>
      <p class="social_p">
        <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://www.instagram.com/alanmckelvey" target="_blank"><i class="fa fa-instagram mx-3"></i></a>
        <a href="https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play mr-3"></i></a>
        <a href="tel:07927114818" target="_blank"><i class="fa fa-phone mr-3"></i></a>
        <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
      </p>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection