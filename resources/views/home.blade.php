@extends('layouts.app')

@section('content')
<div class="container pad mob_no_pad">
    <div class="row pad_small">
        <div class="col-sm-12">
            <h2 class="mb-3">Products</h2>
        </div>
    </div>
    <div class="row"> 
        @foreach($products as $key => $product)
        <div class="col-md-6">
            <div class="dash-product @if($product->bought == false) not-purchased @endif">
                <div class="container">
                    <div class="row">
                        <div class="col-3 p-0 mr-0 square scrollFade mob_marg_btm bg" style="background-image: url({{$product->getFirstMediaUrl('products','big')}});" data-fade="fadeIn">
                        </div>
                        <div class="col-9 ml-0 dash-product-content">
                            <div class="row my-3">
                                <div class="col-12">
                                    <h3 class="scrollFade mb-2" data-fade="fadeIn">{{$product->name}}</h3>
                                    <p class="scrollFade mb-2" data-fade="fadeIn"><b>{{$product->category->name}}</b> &nbsp; -  &nbsp;
                                    <span class="grey_text">
                                    @if($product->bought == true)
                                        Purchased
                                    @else
                                        @if($product->sale_price > 0)
                                        £{{number_format((float)$product->price, 2, '.', '')}}</span> £{{$product->sale_price}}
                                        @else
                                        £{{$product->price}}
                                        @endif
                                    @endif
                                    </span>
                                    </p>
                                    @if($product->bought == true)
                                    <a href="{{$product->getFirstMediaUrl('downloads')}}">
                                        <div class="btn btn-primary mt-0 d-block mr-0 btn-small scrollFade" data-fade="fadeIn">Download</div>
                                    </a>
                                    @else
                                    <a href="/add-to-basket/{{$product->id}}">
                                        <div class="btn btn-primary mt-0 d-block mr-0 btn-small scrollFade" data-fade="fadeIn">Buy Now</div>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach 
    </div>
</div>
@if(count($currentUser->invoices))
<div class="container pad_btm">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="mb-3">Purchase History</h2>
        </div>
        @foreach($currentUser->invoices as $invoice)
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="row light_bg mb-2 pad_small">
                    <div class="col-md-3"><p><b>Id: </b>{{$invoice->transaction_id}}</p></div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <p><b>Purchased:</b> @foreach($invoice->products as $p) {{$p->name}} @endforeach </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"><p><span class="bold">Paid at:</span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($invoice->created_at))->format('g:i a - d/m/Y') }}</p></div>
                    <div class="col-md-2"><p><span class="bold">Price:</span> £{{$invoice->price}}</p></div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif

</div>
@endsection
