@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.news").'">News</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.news.edit', ['news' => $post->id]) .'">Edit</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<edit-news :news="{{$post}}" :image="'{{$post->getFirstMediaUrl('news')}}'" :categories='{{$categories}}'></edit-news>
@endsection
