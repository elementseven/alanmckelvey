@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.news").'">News</a><i class="fa fa-angle-right mx-3"></i><a href="'. route("admin.news.create").'">Create</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<create-news :categories='{{$categories}}'></create-news>
@endsection