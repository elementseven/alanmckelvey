@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.events").'">Events</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.events.edit', ['event' => $event->id]) .'">Edit</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<edit-event :event="{{$event}}" :locations="{{$locations}}" :image="'{{$event->getFirstMediaUrl('events')}}'"></edit-event>
@endsection