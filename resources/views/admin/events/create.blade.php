@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.events").'">Events</a><i class="fa fa-angle-right mx-3"></i><a href="'. route("admin.events.create").'">Create</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<create-event :locations="{{$locations}}"></create-event>
@endsection