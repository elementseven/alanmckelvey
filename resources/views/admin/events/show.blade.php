@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.events").'">Events</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.events.show', ['event' => $event->id]) .'">View</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<show-event :event="{{$event}}" :image="'{{$event->getFirstMediaUrl('events')}}'"></show-event>
@endsection
@section('modals')
<remove-event></remove-event>
@endsection