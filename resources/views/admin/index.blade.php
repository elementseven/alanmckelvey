@php
$bcrumb = '<a href="'.route("home").'">Dashboard</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])

@section('content')
<div class="container my-5">
  <div class="row half_row mb-5">
    <div class="col-md-3 col-6 half_col mb-3">
      <a href="{{route('admin.products.create')}}">
        <div class="card dash-btn">
          <p class="mb-0"><i class="fa fa-archive mr-2"></i> <b>Add Product</b></p>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-6 half_col">
      <a href="{{route('admin.news.create')}}">
        <div class="card dash-btn">
          <p class="mb-0"><i class="fa fa-newspaper-o mr-2"></i> <b>Add News</b></p>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-6 half_col">
      <a href="{{route('admin.events.create')}}">
        <div class="card dash-btn">
          <p class="mb-0"><i class="fa fa-calendar-check-o mr-2"></i> <b>Add Event</b></p>
        </div>
      </a>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-md-12 mb-5">
      <website-visitors :days="7"></website-visitors>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-md-6">
      <top-referrers :days="7"></top-referrers>
    </div>
    <div class="col-md-6">
      <user-types :days="7"></user-types>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mb-5">
      <popular-pages :days="7"></popular-pages>
    </div>
  </div>
</div>
@endsection
