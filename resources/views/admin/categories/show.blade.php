@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.categories").'">Categories</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('categories.show', ['category' => $category->id]) .'">Manage</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<show-category :category="{{$category}}" :subcategories="{{json_encode($category->subcategories)}}"></show-category>
@endsection
@section('modals')
<remove-category></remove-category>
@endsection