@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.categories").'">Categories</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<index-categories></index-categories>
@endsection
@section('modals')
{{-- <remove-category></remove-category> --}}
@endsection