@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.products").'">Products</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.products.edit', ['product' => $product->id]) .'">Edit</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<edit-product :product="{{$product}}" :categories="{{$categories}}" :img="'{{$product->getFirstMediaUrl('products','big')}}'" :download="'{{$product->getFirstMediaUrl('downloads')}}'" ></edit-product>
@endsection
