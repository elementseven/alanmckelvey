@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.products").'">Products</a><i class="fa fa-angle-right mx-3"></i><a href="'. route("admin.products.create").'">Create</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<create-product :categories="{{$categories}}"></create-product>
@endsection