@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.products").'">Products</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.products.show', ['product' => $product->id]) .'">View</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<show-product :product="{{$product}}" :image="'{{$product->getFirstMediaUrl('products','big')}}'" :download="'{{$product->getFirstMediaUrl('downloads')}}'"></show-product>
@endsection
@section('modals')
<remove-product></remove-product>
@endsection