@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.locations").'">Locations</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<index-locations></index-locations>
@endsection
@section('modals')
<remove-location></remove-location>
@endsection