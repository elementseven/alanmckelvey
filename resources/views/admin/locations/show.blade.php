@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.locations").'">Locations</a><i class="fa fa-angle-right mx-3"></i><a href="'. route('admin.locations.show', ['location' => $location->id]) .'">View</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<show-location :location="{{$location}}"></show-location>
@endsection
@section('modals')
<remove-location></remove-location>
@endsection