@php
$bcrumb = '<a href="'.route("admin").'">Dashboard</a><i class="fa fa-angle-right mx-3"></i><a href="'.route("admin.locations").'">Locations</a><i class="fa fa-angle-right mx-3"></i><a href="'. route("admin.locations.create").'">Create</a>';
@endphp
@extends('layouts.admin', ['bcrumb' => $bcrumb])
@section('content')
<create-location></create-location>
@endsection