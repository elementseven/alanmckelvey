<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>

				<p style="font-size:22px; background-color: #14151b; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Order Complete</span><span style="float:right;font-size: 14px;">Ref: {{$reference}}</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$name}},</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thanks for completing your recent order on the Owsey website.<br><b>Your payment reference is <span style="color:#14151b;">{{$reference}}</span></b>.</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Please login and download your purchases.</p>
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b style="width: 150px; display: inline-block;">Purchased:</b></p>
				@foreach($products as $product)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">{{$product->name}}<br></p>
				@endforeach

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b style="width: 150px; display: inline-block;">Total Paid:</b> &pound;{{$total}}</p>
				
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">You can log into your account to download your purchases at any time at <a href="https://owseymusic.com/login" style="color: #14151b;">owseymusic.com/login</a></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">I hope you enjoy the music.</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Kind Regards,<br>Owsey</p>
				
				</td>
				</tr>
				<tr><td><br>
				
				
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif;">This is an automatic email sent from the Owsey website. <a href="https://owseymusic.com">owseymusic.com</a><br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>