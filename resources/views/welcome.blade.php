@php
$page = 'Home';
$pagetitle = "Alan McKelvey Music | One of Belfast, Northern Ireland's premier local musicians";
$metadescription = "A regular on the Belfast music scene since the 1980’s, Alan McKelvey has been entertaining us for decades. His world class vocals and inspired guitar playing provide a unique blues rock experience in Northern Ireland. Seeking influence from Blues greats like Gary Moore, Joe Cocker and BB King, Alan performs an extensive set of classic and modern Blues Rock in his own unique style.";
$pagetype = 'dark';
$pagename = 'home';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])

@section('content')

<div id="home_bg" class="home_bg container-fluid position-relative z-2">
  <div class="trans"></div>
  <div class="d-table position-absolute h-100 pad mob_no_pad">
    <div class="d-table-cell align-middle w-100 h-100">
      <div class="container">
        <div class="row pad_90">
          <div class="col-md-12">
            <div class="scrollFade text-center" data-fade="fadeIn">
              <div id="menu_trigger"></div>
              <picture> 
                <source srcset="/img/logos/logo-large.webp" type="image/webp"/> 
                <source srcset="/img/logos/logo-large.png" type="image/png"/>
                <img id="home_logo" src="/img/logos/logo-large.png" class="d-block w-100 m-auto" alt="Alan McKelvey Music Logo large" />
              </picture>
              <h1 class="text-white home-h1 mt-3">Rhythm &amp; Blues, Rock &amp; Soul</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="top-btns">
      <div class="container">
        <div class="row half_row justify-content-center">
          <div class="col-sm-6 col-lg-4 half_col">
            <a href="/events">
              <div class="top-btn top-btn-primary">What's On?</div>
            </a>
          </div>
          <div class="col-sm-6 col-lg-4 half_col">
            <a href="/contact">
              <div class="top-btn top-btn-white">Request Booking</div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="scrollBtnRow d-none">
    <div class="container">
      <div class="scroll-btn-wrap"></div>
      <div class="scroll-btn scroll">
        <i class="fa fa-angle-down" aria-hidden="true"></i>
      </div>
    </div>
  </div>
</div>
<div class="container py-5 mt-5 mt-4">
  <div class="row scrollTo mt-4">
    <div class="col-lg-6">
      <h2 class="mb-3">Entertaining for decades</h2>
      <p class="bold pb-2 scrollFade" data-fade="fadeIn">A regular on the Belfast music scene since the 1980’s, Alan McKelvey has been entertaining us for decades. His world class vocals and inspired guitar playing provide a unique blues rock experience in Northern Ireland. Seeking influence from Blues greats like Gary Moore, Joe Cocker and BB King, Alan performs an extensive set of classic and modern Blues Rock in his own unique style.</p>
      <p class="scrollFade" data-fade="fadeIn">Alan performs regularly with a host of well known artists in the greater Belfast area and even further afield. Wether it’s with a large 8 piece band or as a solo performance, a night with Alan is sure to be entertaining and not to be missed!</p>
    </div>
    <div class="col-lg-6 mob_small_top scrollFade" data-fade="fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="col-sm-12 video-container">
            <iframe width="560" height="260" src="https://www.youtube.com/embed/R-oC6mbUwc8?rel=0&controls=0&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid py-5">
  <div id="social_stripe" class="row dark_bg text-center py-5 bg">
    <div class="col-sm-12">
      <h2 class="mb-0 scrollFade" data-fade="fadeIn">Follow</h2>
      <p class="mb-3 text-white">Follow on social media or get in touch</p>
      <p class="social_p">
        <a href="https://www.facebook.com/alanmckelveyband/" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://www.instagram.com/alanmckelvey" target="_blank"><i class="fa fa-instagram mx-3"></i></a>
        <a href="https://www.youtube.com/user/lagrange59" target="_blank"><i class="fa fa-youtube-play mr-3"></i></a>
        <a href="tel:07927114818" target="_blank"><i class="fa fa-phone mr-3"></i></a>
        <a href="mailto:info@alanmckelvey.com" target="_blank"><i class="fa fa-envelope-o"></i></a>
      </p>
    </div>
  </div>
</div>
<div class="container py-5">
  <div class="row">
    <div class="col-sm-12 pb-5">
      <h3 class="scrollFade mb-2" data-fade="fadeIn">Upcoming Events</h3>
      <upcoming-events :count="2"></upcoming-events>
      <p class=""><b><a href="{{route('events')}}"><i class="fa fa-chevron-right mr-2"></i> View more upcoming events</a></b></p>
    </div>
  </div>
</div>
<div class="container-fluid py-5" style="background-color: #ebedef;">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 text-center">
          <a href="https://www.thebelfastempire.com/" target="_blank">
            <picture> 
              <source srcset="/img/logos/empire.webp" type="image/webp"/> 
              <source srcset="/img/logos/empire.png" type="image/png"/>
              <img src="/img/logos/empire.png" class="mb-3" width="200" alt="Belfast Empire Logo" />
            </picture>
          </a>
          <p class="quote"><b>"Alan’s soulful vocal is unrivalled on the local scene. His Souldiggers are a band of similarly experienced musicians who have graced the stages of all on the major festival events across the country and internationally."</b></p>
          <p class="quote-name mb-0"><b>Belfast Empire</b></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container py-5">
  <div class="row">
    <div class="col-sm-12 pt-5">
      <h3 class="scrollFade mb-3" data-fade="fadeIn">Latest Blog Posts</h3>
      <div class="row">
        <blog-posts :category="'all'" :count="3" :showpagination="false" :colsize="6"></blog-posts>
      </div>
    </div> 
  </div>
</div>  
@endsection

@section('scripts')

@endsection