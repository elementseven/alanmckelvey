@php
$page = 'Blog';
$pagetitle = 'Blog | '. $blog->title;
$metadescription = "";
$pagetype = 'light';
$pagename = 'blog';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="container scrollTo pad">
    <div class="row pad_top">
        <div class="col-lg-8 pr-5 mob-pr-3">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="mb-2">{{$blog->title}}</h1>
                </div>
                <div class="col-md-10">
                    <p class="large_p"><b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($blog->created_at))->format('jS M Y') }}</b></p>
                    <p class="bold mb-5"><i>{{$blog->exerpt}}</i></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="row">
                <div class="col-12 mb-5">
                    <img src="{{$blog->getFirstMediaUrl('news')}}" alt="{{$blog->title}}" class="w-100">
                </div>
                <div class="col-sm-12 blog_holder img_holder"> 
                    {!!$blog->body!!}
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-4 mob-mt-5 text-second">Other blog Articles</h4>
                </div>
                @foreach($recent as $r)
                <div class="col-12">
                    <a href="/blog/{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('Y-m-d')}}/{{$r->slug}}" class="text-grey">
                        <p class="mb-1"><b>{{$r->title}}</b> <i class="fa fa-angle-right float-right"></i></p>
                        <p class="caption text-grey mb-0">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('d/m/Y')}}</p>
                    </a>
                    <hr>
                </div>
                @endforeach
                <div class="col-12">
                    <a href="{{route('blog')}}">Return to blog page <i class="fa fa-angle-right ml-2"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection
@section('scripts')
@endsection