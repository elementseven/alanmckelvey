@php
$page = 'News';
$pagetitle = 'News | Stay up to date with all the latest Alan McKelvey news';
$metadescription = "Stay up to date with Alan's latest views, opinions, observations and announcements, all in one place. With the music scene moving so fast in Belfast, Alan has his finger on the pulse at the front line of Blues and Rock in the city and surrounding area.";
$pagetype = 'dark';
$pagename = 'news';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<header class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg2.jpg')">
    <div class="trans twoThirdsHeight">
        <div class="dis-table twoThirdsHeight width_100">
            <div class="vert-mid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 white_text">
                            <h1 class="page_title">Blog</h1>
                            <div id="menu_trigger"></div>
                            <p class="max_500">Check out all of my recent blog posts below. Don't forget to check back again soon and keep an eye on my social media for the next post.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container pad">
    <div class="row">
        <blog-posts v-bind:category="'all'" :count="6" :showpagination="true" :colsize="6"></blog-posts>
    </div>
</div>
@endsection

@section('scripts')

@endsection