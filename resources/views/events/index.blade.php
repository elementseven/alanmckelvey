@php
$page = 'Events';
$pagetitle = 'Events | Find all the upcoming Alan McKelvey performances';
$metadescription = "Alan performs regularly across Northern Ireland and further afield with a variety of different musicians and bands. Playing everything from Blues, Soul and Jazz to Classic Rock, you are sure to find something to suit your taste!";
$pagetype = 'dark';
$pagename = 'events';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg1.jpg')">
  <div class="trans twoThirdsHeight">
    <div class="dis-table twoThirdsHeight width_100">
      <div class="vert-mid">
        <div class="container">
          <div class="row">
            <div class="col-md-12 white_text">
              <h1 class="page_title mb-0">Events</h1>
              <div id="menu_trigger"></div>
              <p class="max_500">Find out where you can see Alan next. Don't forget to visit the facebook event and let us know you are coming!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<events></events>
@endsection

@section('scripts')

@endsection