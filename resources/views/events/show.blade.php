@php
$page = 'Events';
$pagetitle = 'Events | Find all the upcoming Alan McKelvey performances';
$metadescription = "Alan performs regularly across Northern Ireland and further afield with a variety of different musicians and bands. Playing everything from Blues, Soul and Jazz to Classic Rock, you are sure to find something to suit your taste!";
$pagetype = 'dark';
$pagename = 'events';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('{{$event->getFirstMediaUrl('events')}}')">
  <div class="trans twoThirdsHeight">
    <div class="dis-table twoThirdsHeight width_100">
      <div class="vert-mid">
        <div class="container">
          <div class="row">
            <div class="col-md-12 white_text">
              <h2 class="page_title mb-0">{{$event->name}}</h2>
              <h3 class="mb-0">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->start)->format('d/m/Y')}} @if($event->end != $event->start) - {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->end)->format('d/m/Y')}}@endif <br>{{$event->time}}</h3>
              <div id="menu_trigger"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container pad">
  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-12"> 
          <h4>About the event</h4>
          {!!$event->description!!}
        </div>
      </div>
      @if($event->link != NULL)
      <div class="row mt-4">
        <div class="col-12">
          <p><b>Let us know you're coming and check out the facebook event</b></p>
          <a href="{{$event->link}}" target="_blank">
            <div class="btn btn-primary ml-0">Facebook Event</div>
          </a>
        </div>
      </div>
      @endif
    </div>
    <div class="col-md-4">
      <h4>{{$event->location->name}}, {{$event->location->city}}</h4>
      <p class="mb-0">{{$event->location->address_line_1}}, {{$event->location->address_line_2}}</p>
      <p>{{$event->location->post_code}}</p>
      <a href="{{$event->location->g_maps}}" target="_blank">View on Google Maps</a>
      <a href="{{$event->location->link}}" target="_blank">
        <div class="btn btn-primary ml-0 mt-3">Visit Website</div>
      </a>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div id="social_stripe" class="row dark_bg text-center py-5 bg" style="background-image: url('/img/bg/bg.jpg');">
    <div class="col-12 text-center">
      <h3 class="text-white">Check out more events!</h3>
      <a href="{{route('events')}}">
        <div class="btn btn-white mt-3">All Events</div>
      </a>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection
