@php
$page = 'Shop';
$pagetitle = 'Shop | Albums, Singles, Artwork and more';
$metadescription = "";
$pagetype = 'dark';
$pagename = 'shop';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg3.jpg')">
    <div class="trans twoThirdsHeight">
        <div class="dis-table twoThirdsHeight width_100">
            <div class="vert-mid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 white_text">
                            <h1 class="page_title">Shop</h1>
                            <div id="menu_trigger"></div>
                            <p class="max_500">Purchase albums, singles and other digital products.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pad">
    <div class="row">
        <div class="col-sm-9 pr-5 mob_pr_0">

            @if(count($products))
            @foreach($products as $key => $product)
            <div class="row product">
                <div class="col-sm-3">
                    <div class="square scrollFade mob_marg_btm" data-fade="fadeIn">
                        <img src="{{$product->getFirstMediaUrl('products','big')}}" class="img_100 square" alt="{{$product->name}}">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-sm-8">
                            <h3 class="scrollFade" data-fade="fadeIn">{{$product->name}}</h3>
                        </div>
                        <div class="col-sm-4">
                            @if($product->sale_price > 0)
                            <h4 class="mb-0 grey_text text-right scrollFade" style="line-height: 32px;" data-fade="fadeIn"><span style="text-decoration: line-through; font-size: 14px;color: #888;">£{{number_format((float)$product->price, 2, '.', '')}}</span> £{{$product->sale_price}}</h4>
                            @else
                            <h4 class="mb-0 grey_text text-right scrollFade" style="line-height: 32px;" data-fade="fadeIn">£{{$product->price}}</h4>
                            @endif
                        </div>
                    </div>
                    <p class="scrollFade" data-fade="fadeIn"><b>{{$product->category->name}}</b></p>
                    <div class="font_16">{!!$product->description!!}</div>
                    <div class="row pt-3">
                        <div class="col-sm-6">
                            <a href="/add-to-basket/{{$product->id}}">
                                <div class="btn btn-primary mt-0 float-left mob_no_float scrollFade" data-fade="fadeIn">Add to basket</div>
                            </a>
                        </div>
                    </div>
                </div>
                @if($key != count($products) -1) 
                <div class="col-md-12">
                    <hr class="my-5">
                </div>
                @endif
            </div>
            @endforeach
            @else
            <div class="row product pad_top">
                <div class="col-sm-12">
                    <h4>Nothing for sale at the minute</h4>
                    <p>I don't have anything up for sale at the minute, be sure to check back later for some cool stuff.</p>
                </div>
            </div>
            @endif
        </div>
        <div class="col-sm-3 basket_holder mobile_hide">
            <div id="mini_basket" class="row">
                <div class="col-sm-12">
                    <h4>Basket</h4>
                    @if(count(Cart::content()))
                    @foreach(Cart::content() as $item)
                    @php
                    foreach($allproducts as $product){
                        if($product->name == $item->name){
                            if($product->sale_discount > 0){
                                $price = $product->price - (($product->price/100) * $product->sale_discount);
                                $price = number_format((float)$price, 2, '.', '');
                            }else{
                                $price = $product->price;
                                $price = number_format((float)$price, 2, '.', '');
                            }
                        }
                    }
                    @endphp
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="small_text">Items can be removed from your basket at the checkout.</p>
                                <p class="font_16 mb-3"><b>£{{$price}} - {{$item->name}}</b></p>
                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{route('basket')}}"><div class="btn btn-primary btn-full">Checkout</div></a>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Your basket is currently empty.</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
    @if($products->links() != "")
    <div class="page_links">

        <div class="container">

          <div class="row">

            <div class="col-md-12">

              <p class="text-center pt-5">Pages</p>

              {{ $products->appends($_GET)->links() }}

            </div>

          </div>

        </div>

    </div>
    @endif
</div>  

@endsection

@section('scripts')

@endsection