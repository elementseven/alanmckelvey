@php
$page = 'Checkout';
$pagetitle = 'Checkout | View the items in your basket and checkout';
$metadescription = "";
$pagetype = 'dark';
$pagename = 'shop';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg1.jpg')">
    <div class="trans twoThirdsHeight">
        <div class="dis-table twoThirdsHeight width_100">
            <div class="vert-mid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 white_text">
                            <h1 class="page_title">Checkout</h1>
                            <div id="menu_trigger"></div>
                            <p class="max_500">Thank you in advance for your support. Proceeds from sales are used to further the project.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(session('duplicate-product'))
<div class="container pad_top text-center">
    <div class="row">
        <div class="col-12">
            <p class="text-danger">You already have {{session('duplicate-product')}} in your basket.</p>
            @php session()->forget('duplicate-product'); @endphp
        </div>
    </div>
</div>
@endif
@if(session('already-purchased'))
<div class="container pad_top text-center">
    <div class="row">
        <div class="col-12">
            <p class="text-danger">You already have already purchased {{session('already-purchased')}}. <a href="/login">Login to the members area</a> to download!</p>
            @php session()->forget('already-purchased'); @endphp
        </div>
    </div>
</div>
@endif
@if(count(Cart::content()) == 0)
<div class="container pad">
  <div class="row text-center">
      <div class="col-md-12 ">
          <img src="/img/icons/empty_basket.svg" width="100px" alt="empty basket" />
          <div class="row">
              <div class="col-md-12">
                  <p class="my-4"><b>Your basket is currently empty.</b></p>
                  <a href="/shop">
                      <div class="btn btn-primary">Shop</div>
                  </a>
              </div>
          </div>
      </div>
  </div>
</div>
@endif
@if(count(Cart::content()))
<div class="container my-5 pb-5">
    <div class="row mt-5">
        <div class="col-12">
            <h2>Products</h2>
            @foreach(Cart::content() as $i)
            <div class="row">
                <div class="col-12"><hr></div>
                <div class="col-4">
                    <p class="mb-0"><b>{{$i->name}}</b></p>
                </div>
                <div class="col-8 text-right">
                    <p class="mb-0"><b>£{{$i->price}}</b></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <a href="/empty-basket"><i class="fa fa-trash-o"></i> Empty Basket</a>
        </div>
        <div class="col-md-6 text-right">
            <h3><span class="text-primary">Total: </span> £{{Cart::total()}}</h3>
        </div>
    </div>
</div>

<div class="container mb-5">
    <div class="row">
        <div class="col-lg-10 mb-4">
            <h2>Checkout</h2>
            <p>Digital products are saved for you in the members area. You can access the members area and download your purchases anywhere with an internet connection.</p>
        </div>
        <div class="col-12">
            <form id="payment-form" class="row" action="/complete-payment" method="POST">
                {{ csrf_field() }}
                <div class="col-12">
                    <input id="nonce" type="hidden" name="nonce" value="{{ old('nonce') }}" required/> 
                    @if ($errors->has('nonce'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nonce') }}</strong>
                        </span>
                    @endif
                </div>
                @if(!Auth::check())
                <div class="col-6 mb-3">
                    <label for="first_name">First Name*</label>
                    <input id="first_name" type="text" name="first_name" class="form-control mb-0" placeholder="First Name*" value="{{ old('first_name') }}" required/> 
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-6 mb-3">
                    <label for="first_name">Last Name*</label>
                    <input id="last_name" type="text" name="last_name" class="form-control mb-0" placeholder="Last Name*" value="{{ old('last_name') }}" required/> 
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-6 mb-3">
                    <label for="email">Email Address*</label>
                    <input id="email" type="text" name="email" class="form-control mb-0" placeholder="Email Address*" value="{{ old('email') }}" required/> 
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-6 mb-3">
                    <label for="country">Country*</label>
                    <countries></countries>
                    @if ($errors->has('country'))
                        <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-12 mb-4">
                  <div id="agree_holder" class="d-block">
                    <label for="agree" class="text-small">
                      <input id="agree" class="mr-1" type="checkbox" name="agree"/>
                      By ticking this box you give your consent for the personal information entered on this form to be stored in a secure database. Your information will not be shared or used for marketing and is only stored so you may log in and continue to use the features of this application. You may remove your information from our database by contacting me at any time. You also agree to the <a href="https://alanmckelvey.com/tandcs/" target="_blank">Terms &amp; conditions and Privacy Policy</a>.
                    </label>
                    @if ($errors->has('agree'))
                        <span class="help-block">
                            <strong>{{ $errors->first('agree') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                @else
                <div class="col-12">
                  <p class="mb-0">You are logged in as <b>{{$currentUser->full_name}}</b>.<br>Products will be added to your account.</p>
                  <p class="mb-5"><a href="/not-you">Not you? Click here to log out.</a></p>
                </div>
                @endif
            </form>
            <div id="dropin-container"></div>
            <div class="mt-4">
              <p class="text-danger">{!!session('payment_error')!!}</p>
              @php session()->forget('payment_error'); @endphp
              @if(!Auth::check())
              <p id="form-warning" class="text-small text-right mb-1 text-primary">Please complete the form</p>
              @endif
              <div id="submit-button" class="btn btn-primary float-right">Pay Now</div>
            </div>
        </div>
    </div>
</div>

@endif
@endsection

@section('scripts')

<!-- Load the Drop in UI component.. -->
<script src="https://js.braintreegateway.com/web/dropin/1.16.0/js/dropin.min.js"></script>

<script>
  var button = document.querySelector('#submit-button');
  braintree.dropin.create({
    authorization: '{{$client_token}}',
    container: '#dropin-container',
    paypal: {
      flow: 'vault'
    },
    card: {
      cvv: {
        required: true
      }
    },
    threeDSecure: {
      amount: '{{Cart::total()}}',
    }
  }, function (createErr, instance) {
    button.addEventListener('click', function () {
      instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
        // Submit payload.nonce to your server
        $('#nonce').val(payload.nonce);
        $('#payment-form').submit();
      });
    });
  });
</script>
@if(!Auth::check())
<script>
  function checkInputs(){
    if(
      $('#first_name').val() != "" &&
      $('#last_name').val() != "" &&
      $('#email').val() != "" &&
      $('#agree').prop('checked') == true
    ){
      document.getElementById("submit-button").disabled = false;
      $('#form-warning').hide();
    }else{
      document.getElementById("submit-button").disabled = true;
      $('#form-warning').show();
    }
    console.log('fn');
  }

  $('#first_name').keyup(function() {
    checkInputs();
  });
  $('#last_name').keyup(function() {
    checkInputs();
  });
  $('#email').keyup(function() {
    checkInputs();
  });
  $('#agree_holder').click(function() {
    checkInputs();
  });
  $(document).ready(function() {
    checkInputs();
  });
</script>
@endif
@endsection
