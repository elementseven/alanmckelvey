@php
$page = 'Checkout';
$pagetitle = 'Checkout | Complete your purchase';
$metadescription = "";
$pagetype = 'dark';
$pagename = 'checkout';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')

@php 
    
    $total = Cart::total();
    if(!is_numeric($total)){
        $total = str_replace(',', '', $total);
    }
    $tax = ($total / 100) * 20; 
    $subtotal = $total - $tax;


@endphp

@section('content')
<div class="bg twoThirdsHeight" style="background-image: url('/img/bg/bg1.jpg')">
    <div class="trans twoThirdsHeight">
        <div class="dis-table twoThirdsHeight width_100">
            <div class="vert-mid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 white_text">
                            <h1 class="page_title">Checkout</h1>
                            <div id="menu_trigger"></div>
                            <p class="max_500">Complete your purchase.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pad">
    <div class="row mob_no_pad">
        <div class="col-sm-12">
            <form @if($user != "no") action="/create-payment" @else  action="/create-payment-new-user" @endif method="post">
                        {{ csrf_field() }}
                <div class="row">
                    @if($user == "no")
                    <div class="col-md-12 mt-0">
                        <h1 class="mb-3">Your Details</h1>
                    </div>
                    <div class="col-md-6 mb-3">
                        <p class="bold">First Name</p>
                        <input class="form-control mb-0" id="first_name" name="first_name" type="text" placeholder="First Name" value="{{old('first_name')}}" />
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 mb-3">
                        <p class="bold">Last Name</p>
                        <input class="form-control mb-0" id="last_name" name="last_name" type="text" placeholder="Last Name" value="{{old('last_name')}}" />
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-12 mb-3">
                        <p class="bold">Email Address</p>
                        <input class="form-control mb-0" id="email" name="email" type="text" placeholder="Your Email Address" value="{{old('email')}}" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    @else
                    <div class="col-md-12 pad_small">
                        <h2>Your Details</h2>
                    </div>
                    
                        <input class="form-control" id="first_name" name="first_name" type="hidden" placeholder="First Name" value="{{$user->first_name}}" />
                        <input class="form-control" id="last_name" name="last_name" type="hidden" placeholder="Last Name" value="{{$user->last_name}}" />
                        <input class="form-control" id="email" name="email" type="hidden" placeholder="Your Email Address" value="{{$user->phone}}"/>

                    <div class="col-md-12 pad_small_btm">
                        <p><b>Name:</b> {{$user->first_name}} {{$user->last_name}}</p>
                        <p><b>Email:</b> {{$user->email}}</p>
                    </div>
                    <div class="col-md-12 pad_small_btm">
                        <p>Not you? <a href="/not-you">Click Here</a></p>
                    </div>
                    @endif
                    <div class="col-md-12 mt-4">
                        <p class="text-left"><b>Notes</b></p>
                        <p class="small_text justify">When you complete your payment an account will be created for you if you don't already have one. You will recieve an email with instructions on how to access your account and your password. You will be able to download your purchases from your account.<br>Your account is created so you may access your purchases at any time, from any location. Downloads of your purchases are unlimited. Should you wish to delete your account you can request this at any time by emailing <a href="mailto:info@alanmckelvey.com">info@alanmckelvey.com</a> with the subject line - 'Delete my account'. When you do this your details and all your purchase history will be removed from the Alan Mckelvey Music system and you will no longer be able to access your purchases. Your personal information will not be shared with any third parties and is kept on a secure database. This website complies with current GDPR regulations.</p>
                    </div>
                    <div class="col-md-12 mt-4">
                        <h4>Payment</h4>
                        <hr class="form_line"/>
                    </div>
                    <div class="col-md-12">
                         <h1 class="float-right mob_no_float"><span class="caption dark_text">Total </span><span class="primary_text">£{{Cart::total()}}</span></h1>
                     </div>
                    <div class="col-md-12 mob_marg_btm">
                        <button id="paypal-button" class="btn btn-primary float-right mob_no_float mt-3">Pay With PayPal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')


@endsection
