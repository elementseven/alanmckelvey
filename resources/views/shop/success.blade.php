@php
$page = 'Success';
$pagetitle = 'Success | Thank you for completing your purchase';
$metadescription = "";
$pagetype = 'dark';
$pagename = 'success';
@endphp
@extends('layouts.front', ['page' => $page, 'pagetitle' => $pagetitle, 'metadescription' => $metadescription, 'pagetype' => $pagetype, 'pagename' => $pagename, ])
@section('content')

<div class="container pad_90">

    <div class="row pad_top mob_small_pad">
        <div class="col-sm-12 text-left">
            <div class="row">
                <div class="col-sm-8">
                    <h4 class="pad_small_btm">Thank you for your payment</h4>
                </div>
                <div class="col-sm-4">
                    <button id="print_page" type="print" class="btn btn-primary float-right noprint">Print this page</button>
                </div>
            </div>
            <p>You will receive an email confirming your order details shortly. Please check your inbox and your spam folder.</p>
            <p class="noprint">Please print and keep this page or keep a note of the payment reference.</p>
            <p class="">Please remember to download your purchases from <a href="/login">your account</a> on the Alan McKelvey website.</p>
        </div>
    </div>

    <div class="row pad_top mob_small_pad">
        <div class="col-sm-12 text-left">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h4 class="mob_smaller">Order Details</h4>
                </div>
                <div class="col-sm-8 col-xs-12">
                     <h4 class="red_text text-right ref_no mob_smaller">Ref: {{$invoice->transaction_id}}</h4>
                 </div>
             </div>
            <hr class="line_full">
            <div class="row">
                
                <div class="col-sm-6">

                    <div class="row pad_small_top">
                        <div class="col-sm-5"><p class="bold">Price:</p></div>
                        <div class="col-sm-7"><p>£{{$invoice->price}}</p></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5"><p class="bold">Payent Method:</p></div>
                        <div class="col-sm-7"><p>@if($invoice->card_token != "paypal") Credit/Debit Card @else PayPal @endif</p></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5"><p class="bold">Order made:</p></div>
                        <div class="col-sm-7"><p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($invoice->created_at))->format('g:i a - d/m/Y') }}</p></div>
                    </div>

                </div>
            
                <div class="col-sm-6 text-right your_details_success">
                    <p class="bold">Your Details</p>
                    <p>{{$currentUser->first_name}} {{$currentUser->last_name}}</p>
                    <p>{{$currentUser->email}}</p>
                </div>
            </div>
            <hr class="line_full">
            <div class="row">
                <div class="col-sm-12 pad_small_top">
                    <h4 class="pad_small_btm">Your Purchases</h4>
                    <div class="row">
                        @foreach($invoice->products as $key => $product)
                        <div class="col-md-6">
                            <div class="row product @if($product->bought == true) purchased @endif ">
                                <div class="col-sm-3">
                                    <div class="square scrollFade mob_marg_btm" data-fade="fadeIn">
                                        <img src="{{$product->getFirstMediaUrl('products','big')}}" class="img_100 square" alt="{{$product->name}}">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <p class="scrollFade large_p" data-fade="fadeIn"><b>{{$product->name}}</b></p>
                                        </div>
                                        <div class="col-sm-4">
                                            @if($product->sale_price > 0)
                                            <h4 class="mb-0 grey_text text-right scrollFade" style="line-height: 32px;" data-fade="fadeIn"><span style="text-decoration: line-through; font-size: 14px;color: #888;">£{{number_format((float)$product->price, 2, '.', '')}}</span> £{{$product->sale_price}}</h4>
                                            @else
                                            <h4 class="mb-0 grey_text text-right scrollFade" style="line-height: 32px;" data-fade="fadeIn">£{{$product->price}}</h4>
                                            @endif
                                        </div>
                                    </div>
                                    <p class="scrollFade grey_text" data-fade="fadeIn"><b>{{$product->category->name}}</b></p>
                                </div>
                            </div>
                        </div>
                        @endforeach 
                        <div class="col-md-12">
                            <hr class="my-5">
                        </div>
                    </div>
                    <p class="text-right">To download your purchase's please login to <a href="/login">your account</a>.</p>
                    <a href="/login">
                        <div class="btn btn-primary float-right">Login</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('scripts')

<script>
    $("#print_page").click(function(){
        window.print();
    });
</script>
<!-- Event snippet for Website purchase conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-755439225/CTAyCI-GkZcBEPmsnOgC',
      'value': {{$invoice->price}},
      'currency': 'GBP',
      'transaction_id': '{{$invoice->transaction_id}}'
  });
</script>
@endsection
