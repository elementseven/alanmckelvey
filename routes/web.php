<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin middlware
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function(){

	Route::get('/admin-dashboard', 'AdminController@index')->name('admin');

	Route::get('/admin/news', 'PostController@index')->name('admin.news');
	Route::get('/admin/news/create', 'PostController@create')->name('admin.news.create');
	Route::get('/admin/news/show/{post}/', 'PostController@show')->name('admin.news.show');
	Route::get('/admin/news/edit/{post}/', 'PostController@edit')->name('admin.news.edit');

	Route::get('/admin/events', 'EventController@index')->name('admin.events');
	Route::get('/admin/events/create', 'EventController@create')->name('admin.events.create');
	Route::get('/admin/events/show/{event}/', 'EventController@show')->name('admin.events.show');
	Route::get('/admin/events/edit/{event}/', 'EventController@edit')->name('admin.events.edit');

	Route::get('/admin/locations', 'LocationController@index')->name('admin.locations');
	Route::get('/admin/locations/create', 'LocationController@create')->name('admin.locations.create');
	Route::get('/admin/locations/show/{location}/', 'LocationController@show')->name('admin.locations.show');
	Route::get('/admin/locations/edit/{location}/', 'LocationController@edit')->name('admin.locations.edit');

	Route::get('/admin/categories', 'CategoryController@index')->name('admin.categories');
	Route::get('/admin/categories/show/{category}/', 'CategoryController@show')->name('admin.categories.show');

	Route::get('/admin/products', 'ProductController@index')->name('admin.products');
	Route::get('/admin/products/create', 'ProductController@create')->name('admin.products.create');
	Route::get('/admin/products/show/{product}/', 'ProductController@show')->name('admin.products.show');
	Route::get('/admin/products/edit/{product}/', 'ProductController@edit')->name('admin.products.edit');

});

Route::get('/', 'PageController@index')->name('welcome');
Route::get('/media', 'PageController@media')->name('media');
Route::get('/blog', 'PageController@blog')->name('blog');
Route::get('/blog/{blogdate}/{blogname}', 'PageController@blogSingle')->name('article');
Route::get('/shop', 'PageController@shop')->name('shop');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::get('/not-you', 'HomeController@checkout_logout')->name('not-you');
Route::post('/send-enquiry', 'SendMail@enquiry')->name('send-enquiry');

Route::get('/events', 'PageController@events')->name('events');
Route::get('/events/view/{event}', 'PageController@eventShow')->name('events.show');

Route::get('/basket', 'ShopController@basket')->name('basket');
Route::get('/checkout', 'ShopController@checkout')->name('checkout');
Route::get('/add-to-basket/{product}', 'ShopController@addProductToBasket')->name('add-to-basket');
Route::get('/empty-basket', 'ShopController@emptyBasket')->name('empty-basket');
Route::post('/remove-from-basket/{product}', 'ShopController@remove_item')->name('remove-from-basket');
Route::post('/complete-payment', 'ShopController@completePayment')->name('complete-payment');
Route::get('/success', 'ShopController@success')->name('success');

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();