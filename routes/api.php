<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/events/store', 'EventController@store')->name('events-store');
Route::post('/events/update/{event}/', 'EventController@update')->name('events-update');
Route::get('/events/get/{limit}', 'EventController@getEvents')->name('events-get');
Route::post('/events/del/{event}/', 'EventController@destroy')->name('events-destroy');

Route::post('/locations/store', 'LocationController@store')->name('locations-store');
Route::post('/locations/update/{location}/', 'LocationController@update')->name('locations-update');
Route::get('/locations/get', 'LocationController@getLocations')->name('locations-get');
Route::post('/locations/del/{location}/', 'LocationController@destroy')->name('locations-destroy');

Route::post('/news/store', 'PostController@store')->name('news-store');
Route::post('/news/update/{post}/', 'PostController@update')->name('news-update');
Route::get('/news/get-posts/{category}/{limit}', 'PostController@getPosts')->name('get-posts');
Route::post('/news/del/{post}/', 'PostController@destroy')->name('news-destroy');

Route::post('/categories/store/{category}/', 'CategoryController@store')->name('categories-store');
Route::get('/categories/get/{limit}', 'CategoryController@getCategories')->name('categories-get');
Route::get('/categories/subcategories/get/{category}', 'CategoryController@getSubcategories')->name('subcategories-get');
Route::get('/categories/del/{subcategory}/', 'CategoryController@destroy')->name('categories-destroy');

Route::post('/products/store', 'ProductController@store')->name('products-store');
Route::post('/products/update/{product}/', 'ProductController@update')->name('products-update');
Route::get('/products/get/{limit}', 'ProductController@getProducts')->name('products-get');
Route::post('/products/del/{product}/', 'ProductController@destroy')->name('products-destroy');
Route::get('/products/search', 'ProductController@searchProducts')->name('products-search');

Route::get('/analytics/lastweek', 'AnalyticsController@lastweek')->name('analytics-lastweek');
Route::get('/analytics/user-type', 'AnalyticsController@userTypes')->name('analytics-user-types');
Route::get('/analytics/top-referrers', 'AnalyticsController@topReferrers')->name('analytics-top-referrers');
Route::get('/analytics/popular-pages', 'AnalyticsController@popularPages')->name('analytics-popular-pages');