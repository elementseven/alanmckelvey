<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Product extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $fillable = [
        'name', 'price', 'sale_price', 'description','status','category_id'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('big')->width(600)->sharpen(10);
        $this->addMediaConversion('mobile')->width(368)->sharpen(10);
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function invoices(){
        return $this->belongsToMany('App\Invoice', 'invoice_product')->withPivot('invoice_id');
    }
    public function users(){
        return $this->belongsToMany('App\User', 'product_user')->withPivot('user_id');
    }
}
