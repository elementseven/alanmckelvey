<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class ProductController extends Controller
{

    // Return products as an array
    public function getProducts($limit){
        $products = Product::orderBy('name','asc')->with('category')->paginate($limit);
        return $products;
    }

    // Search for products by name
    public function searchProducts(Request $request){
        $search = $request->input('search');
        $products = Product::where('name','LIKE','%'.$search.'%')->get();
        return $products;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'category' => 'required',
            'status' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'image' => 'required',
            'download' => 'required'
        ]);
        if (
            $request->hasFile('image') && 
            $request->hasFile('download') && 
            $request->file('image')->isValid() && 
            $request->file('download')->isValid()
        ){
            $product = Product::create([
                'name' => $request->input('name'),
                'price' => floatval($request->input('price')),
                'category_id' => $request->input('category'),
                'status' => $request->input('status'),
                'description' => $request->input('description'),
            ]);

            $product->addMediaFromRequest('image')->toMediaCollection('products', 'media');
            $product->addMediaFromRequest('download')->toMediaCollection('downloads', 'media');
            if($request->input('sale_price') != 0){
                $product->sale_price = floatval($request->input('sale_price'));
            }else{
                $product->sale_price = NULL;
            }
            $product->save();
        }
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product = Product::where('id',$product->id)->with('media')->with('category')->first();
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::get();
        $product = Product::where('id',$product->id)->with('media')->with('category')->first();
        return view('admin.products.edit', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'category' => 'required',
            'status' => 'required',
            'price' => 'required|numeric',
            'description' => 'required'
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid() && $request->file('image')->getClientOriginalName() != "") {
            $product->media->where('collection_name','products')->each->delete();
            $product->addMediaFromRequest('image')->toMediaCollection('products', 'media');
        }
        if ($request->hasFile('download') && $request->file('download')->isValid() && $request->file('download')->getClientOriginalName() != "") {
            $product->media->where('collection_name','downloads')->each->delete();
            $product->addMediaFromRequest('download')->toMediaCollection('downloads', 'media');
        }

        $update = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'status' => $request->input('status'),
            'category_id' => $request->input('category'),
            'description' => $request->input('description'),
        ];

        $product->update($update);

        if($request->input('sale_price') != 0){
            $product->sale_price = floatval($request->input('sale_price'));
        }else{
            $product->sale_price = NULL;
        }
        $product->save();

        return floatval($request->input('sale_price'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->media->each->delete();
        $product->delete();
        return "success";
    }
}
