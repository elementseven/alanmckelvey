<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    // Return jobs as an array
    public function getLocations(){
        $locations = Location::orderBy('name','desc')->paginate(200);
        return $locations;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.locations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'city' => 'required|string|max:255'
        ]);

        $location = Location::create([
            'name' => $request->input('name'),
            'city' => $request->input('city'),
            'address_line_1' => $request->input('address_line_1'),
            'address_line_2' => $request->input('address_line_2'),
            'post_code' => $request->input('post_code'),
            'g_maps' => $request->input('g_maps'),
            'link'=> $request->input('link')
        ]);

        return $location;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return view('admin.locations.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('admin.locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'city' => 'required|string|max:255'
        ]);

        $address_line_1 = $request->input('address_line_1');
        $address_line_2 = $request->input('address_line_2');
        $post_code = $request->input('post_code');
        $g_maps = $request->input('g_maps');
        $link = $request->input('link');

        if($address_line_1 == "null"){
            $address_line_1 = NULL;
        }if($address_line_2 == "null"){
            $address_line_2 = NULL;
        }if($post_code == "null"){
            $post_code = NULL;
        }if($g_maps == "null"){
            $g_maps = NULL;
        }if($link == "null"){
            $link = NULL;
        }

        $update = [
            'name' => $request->input('name'),
            'city' => $request->input('city'),
            'address_line_1' => $address_line_1,
            'address_line_2' => $address_line_2,
            'post_code' => $post_code,
            'g_maps' => $g_maps,
            'link'=> $link
        ];

        $location->update($update);

        return $location;
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        return "success";
    }
}
