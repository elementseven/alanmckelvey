<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class SendMail extends Controller
{
    public function enquiry(Request $request){

    	// Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'subject' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = $request->input('subject');
		$name = $request->input('name');
		$country = $request->input('country');
		$email = $request->input('email');
		$content = $request->input('message');
	    
	    if($request->has('phone')){
		    $phone = $request->input('phone');
		}else{
			$phone = NULL;
		}

        Mail::send('emails.enquiry',[
	        'name' => $name,
	        'phone' => $phone,
	        'subject' => $subject,
	        'country' => $country,
	        'email' => $email,
	        'content' => $content
	        ], function ($message) use ($subject, $email, $name, $phone, $content, $country){
	            $message->from('donotreply@alanmckelvey.com', 'Alan McKelvey');
				$message->subject($subject);
				$message->replyTo($email);
				$message->to('luke@icon-creative.com');
			}
		);
        return 'success';
    }
}
