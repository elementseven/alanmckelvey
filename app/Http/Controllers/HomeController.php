<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->role->slug == 'admin'){
            return redirect()->to('/admin-dashboard');
        }

        $products = Product::get();
        foreach ($products as $p) {
            foreach($p->users as $u){
                if($user->id == $u->id){
                    $p->bought = true;
                }else{
                    $p->bought = false;
                }
            }
        }

        return view('home', compact('products'));
    }

    public function logout(){
        Auth::logout();
        return redirect()->to('/login');
    }
    public function checkout_logout(){
        Auth::logout();
        return redirect()->to('/basket');
    }
}
