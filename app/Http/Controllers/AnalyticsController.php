<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;	
use Analytics;
use Spatie\Analytics\Period;

class AnalyticsController extends Controller
{
	// Return the total number of visitors each day for defined amount of days
    public function lastweek(Request $request){
    	$days = $request->input('days');
    	$data = [
    		['Date','Visitors'],
    	];

    	$startDate = Carbon::now()->subDays($days);
		$endDate = Carbon::now();
		$lastweek = Period::create($startDate, $endDate);

		$last_weeks_vists = Analytics::fetchTotalVisitorsAndPageViews($lastweek);

		foreach($last_weeks_vists as $day){
			$date = Carbon::createFromFormat('Y-m-d H:i:s', $day['date'])->format('d M');
			$theday = [$date, $day['visitors'] ];
			array_push($data, $theday);
		}

    	return $data;

    }

    // Return the new user / returning user split for defined amount of days
    public function userTypes(Request $request){
    	$days = $request->input('days');
    	$data = [
    		['User Type','Sessions'],
    	];
		$userTypes = Analytics::fetchUserTypes(Period::days($days));

		foreach($userTypes as $day){
			$add = [$day['type'], $day['sessions']];
			array_push($data, $add);
		}

    	return $data;

    }

    // Return the top 10 referrers for defined amount of days
    public function topReferrers(Request $request){
    	$days = $request->input('days');
		$userTypes = Analytics::fetchTopReferrers(Period::days($days), 10);
    	return $userTypes;
    }

    // Return the top 10 most popular pages for defined amount of days
    public function popularPages(Request $request){
    	$days = $request->input('days');
		$popularPages = Analytics::fetchMostVisitedPages(Period::days($days), 10);  
    	return $popularPages;
    }
}
