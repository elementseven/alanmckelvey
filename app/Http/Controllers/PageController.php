<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Category;
use App\Product;
use App\Event;
use App\Blog;
use App\Post;

class PageController extends Controller
{
    public function index()
    {
        $events = Event::get();
        $blogs = Post::where('status','PUBLISHED')->orderBy('created_at','desc')->take(2)->get();
        return view('welcome', compact('blogs','events'));
    }
    public function about(){
        return view('about');
    }
    public function events()
    {
        $events = Event::get();
        return view('events.index', compact('events'));
    }
    public function eventShow(Event $event)
    {
        return view('events.show', compact('event'));
    }
    public function media()
    {
        return view('media');
    }
    public function blog()
    {
        $categories = Category::get();
        $blogs = Post::where('status','PUBLISHED')->orderBy('created_at','desc')->paginate(6);
        return view('news.index', compact('blogs', 'categories'));
    }
    public function blogSingle($newsdate, $newsslug)
    {
        $newsdate = Carbon::createFromFormat('Y-m-d', $newsdate)->toDateString();
        $blog = Post::whereDate('created_at',$newsdate)->where('slug',$newsslug)->first();
        $recent = Post::where('id','!=',$blog->id)->orderBy('created_at','desc')->paginate(6);

        return view('news.single', compact('blog','recent'));
    }
    public function shop()
    {
        $allproducts = Product::get();
        $products = Product::where("status","Active")->orderBy('created_at','desc')->paginate(6);
        return view('shop.index', compact('products','allproducts'));
    }
    public function contact()
    {
        return view('contact');
    }
    public function send(Request $request)
    {
        $this->validate($request,[
            
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required',
            'message' => 'required'
            
        ]);
        
        $name = $request->input('name');
        $email = $request->input('email');
        $yourmessage = $request->input('message');
        $phone = "No phone number provided";
        $thesubject = $request->input('subject');

        if($request->input('phone') != NULL){
            $phone = $request->input('phone');
        }
        
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $thesubject,
            'email' => $email,
            'phone' => $phone,
            'yourmessage' => $yourmessage
            ], function ($message) use ($thesubject, $email){
                $message->from('donotreply@owseymusic.com', 'Owsey');
                $message->subject($thesubject);
                $message->replyTo($email);
                $message->to('owseymusic@gmail.com');
            }
        );
        
        session(['message_sent' => "Message sent"]);
        
        return redirect('/contact');
        
    }
    public function getEvents(Request $request){
        // Validate the form data
        $this->validate($request,[
            'date' => 'required|string'
        ]);
        $thedate = Carbon::createFromFormat('Y-m-d', $request->input('date'));
        $events = Event::whereDate('date', '=', $thedate->toDateString())->orderBy('start','asc')->get();
        foreach($events as $e){
            $e->thedate = Carbon::createFromFormat('Y-m-d H:i:s', $e->date)->format('jS');
            $e->themonth = Carbon::createFromFormat('Y-m-d H:i:s', $e->date)->format('M');
        }
        if(!count($events)){
            $events ='No events';
        }

        return $events;
    }
}
