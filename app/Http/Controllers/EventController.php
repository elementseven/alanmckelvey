<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Location;
use App\Event;

class EventController extends Controller
{

    // Return jobs as an array
    public function getEvents(Request $request, $limit){
        $search = $request->input('search');
        $now = Carbon::now();
        $events = Event::whereDate('date','>=', $now)
        ->where(function($query) use ($search){
            $query->where('name','LIKE','%'.$search.'%');
            $query->orWhereHas("location", function($q) use($search){
                if($search != ''){
                    $q->where('name','LIKE','%'.$search.'%')
                    ->orWhere('city','LIKE','%'.$search.'%');
                }
            });
        })
        ->orderBy('date','asc')
        ->with('location')
        ->paginate($limit);
        return $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::get();
        return view('admin.events.create', compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'year' => 'required|numeric',
            'month' => 'required|numeric',
            'date' => 'required|numeric',
            'time' => 'required|string|max:255',
            'description' => 'required',
        ]);
        $event = Event::create([
            'name' => $request->input('name'),
            'date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('year') .'-'. $request->input('month') .'-'. $request->input('date') . ' 00:00:00'),
            'time' => $request->input('time'),
            'description' => $request->input('description'),
            'location_id' => $request->input('location'),
            'link'=> $request->input('link')
        ]);
        return $event;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $event = Event::where('id',$event->id)->with('media')->first();
        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $locations = Location::get();
        $event = Event::where('id', $event->id)->with('location')->first();
        return view('admin.events.edit', compact('event','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'year' => 'required|numeric',
            'month' => 'required|numeric',
            'date' => 'required|numeric',
            'time' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $link = $request->input('link');
        if($link == "null"){
            $link = NULL;
        }

        $update = [
            'name' => $request->input('name'),
            'date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('year') .'-'. $request->input('month') .'-'. $request->input('date') . ' 00:00:00'),
            'time' => $request->input('time'),
            'description' => $request->input('description'),
            'location_id' => $request->input('location'),
            'link'=> $link
        ];

        $event->update($update);

        return $event;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        return "success";
    }
}
