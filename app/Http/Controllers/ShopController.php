<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Braintree_WebhookNotification;
use Braintree_PaymentMethod;
use Braintree_Subscription;
use Braintree_ClientToken;
use Braintree_Transaction;
use Braintree_CreditCard;
use Braintree_Customer;

use App\Product;
use App\Invoice;
use App\User;
use App\Card;

use Cart;
use Mail;
use Auth;

class ShopController extends Controller
{
	protected $provider;

    public function basket(){
    	if(Auth::check()){
    		$user = Auth::user();
    		$client_token = Braintree_ClientToken::generate([
			    "customerId" => $user->customer_id
			]);
    	}else{
    		$client_token = Braintree_ClientToken::generate();
    	}
    	
    	if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
        	$invoice = NULL;
        }
        return view('shop.basket',compact('client_token','invoice'));
    }

    public function success(){
    	if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
            session()->forget('invoice_id');
        }else{
        	return redirect()->to('login');
        }
    	return view('shop.success',compact('invoice'));
    }

    public function emptyBasket(){
        if(session('invoice_id') != NULL){
            Cart::destroy();
            $invoice = Invoice::where("id",session('invoice_id'))->first();
            $invoice->delete();
            session()->forget('invoice_id');
        }
        return redirect()->to("/basket");
    }

    public function addProductToBasket(Request $request, Product $product)
    {

    	if(Auth::check()){
    		$user = Auth::user();
    		foreach($product->users as $u){
                if($user->id == $u->id){
                    $request->session()->put('already-purchased', $product->name);
                    return redirect()->to('/basket');
                }
            }
    	}

        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
            $request->session()->forget('invoice_id');
            // Create invoice for user 
            $invoice = $this->createInvoice(NULL, NULL, 0, 0);
            $request->session()->put('invoice_id', $invoice->id);
        }

        $hasProduct = $invoice->products()->where('id', $product->id)->exists();

        if(!$hasProduct){
            // Attach group to invoice and add to basket
            $invoice->products()->attach($product->id);

            Cart::add( $product->id, $product->name, 1, $product->price);

            $this->updateTotal($invoice);
            
            $items = Cart::content();
            $tax_rate = 0;
            config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'

            foreach ($items as $item){
                $item->setTaxRate($tax_rate);
                Cart::update($item->rowId, $item->qty);
            }
            return redirect()->to('/basket');
        }else{
            $request->session()->put('duplicate-product', $product->name);
        }

        return redirect()->to('/basket');
    }

    public function completePayment(Request $request){

    	if(Auth::check()){
    		// Validate the form data
	        $this->validate($request,[
	            'nonce' => 'required'
	        ]);
        	$user = Auth::user();
    	}else{
    		// Validate the form data
	        $this->validate($request,[
	            'first_name' => 'required|string|max:255',
	            'last_name' => 'required|string|max:255',
	            'email' => 'required|string|email|max:255|unique:users',
	            'agree' => 'accepted',
	            'nonce' => 'required',
	            'country' => 'required|string|max:255',
	        ]);
    	}

        // Create Variables for use in the transaction
        $total = Cart::total();
        $nonce = $request->input('nonce');
        $subscribed= false;

        if(Auth::check()){
    		$customer_id = $user->customer_id;
    	}else{
    		$customer_id = $this->registerUserOnBrainTree($request->input('first_name'),$request->input('last_name'), $request->input('email'));
    	}
        session(['customer_id' => $customer_id]);
        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
            $request->session()->forget('invoice_id');
            // Create invoice for user 
            $invoice = $this->createInvoice($customer_id, NULL, $total, 0);
            $request->session()->put('invoice_id', $invoice->id);
        }

        // Perform the transaction and request the Transaction ID
        $transaction_id = $this->createTransaction($nonce, $customer_id, "", $subscribed, $total, $invoice->id);

        // If there was an error processing the payment
        if($transaction_id == "error"){
            session(['general_payment_error' => "There was an error processing your payment."]);
            return back();
        }

        // If the transaction ID is set
        if($transaction_id != ""){
        	if(Auth::check()){
        		$user = Auth::user();
        	}else{
        		$password = $this->randomPassword();
        		$user = $this->createUser($request->input('first_name'),$request->input('last_name'), $request->input('email'), $request->input('country'), $password, $customer_id);
        	}

            $invoice->paid = true;
            $invoice->transaction_id = $transaction_id;
            $invoice->user_id = $user->id;
            $invoice->save();

            foreach(Cart::content() as $i){
                $user->products()->attach($i->id);
            }

            // Send payment confirmation email
            // $this->paymentConfirmed($invoice, $user, $password);

            // Redirect to my account page
            Cart::destroy();
            session(['successful_payment' => $invoice->id]);
            return redirect()->to('/success');
       
        }else{
            // Inform the user of the failed transaction
            session(['general_payment_error' => "There was an error processing your payment."]);
            return back();
        }

    }

    public function updateTotal(Invoice $invoice){
        $productsTotal = 0;
        foreach($invoice->products as $p){
            $productsTotal = $productsTotal + $p->price;
        }
        $invoice->price = $productsTotal;
        $invoice->save();
    }

    // Function to and create a new user in Braintree 
    public function registerUserOnBrainTree($first_name, $last_name, $email){

        // Pass the user information to braintree
        $result = Braintree_Customer::create(array(
            'firstName' => $first_name,
            'lastName' => $last_name,
            'email' => $email
        ));

        if ($result->success) {

            // Return the Customer ID
            return $result->customer->id;
        }else{

            // Create empty variable to store errors
            $errorFound = '';

            // Loop through errors and add them to the error string
            foreach ($result->errors->deepAll() as $error) {
                $errorFound .= $error->message . "<br />";
            }

        }
    }


    // Function to create a random password
	public function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

    // Function to create a user
    public function createUser($first_name, $last_name, $email, $country, $password, $customer_id){

        $user = User::create([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'country' => $country,
            'password' => Hash::make($password),
            'customer_id' => $customer_id,
            'role_id' => 2
        ]);

        Auth::login($user, true);
        return $user;

    }

    // Function to create a new invoice and add it to the user
    public function createInvoice($customer_id, $transaction_id, $price, $paid){

        // Create a new invoice object
        $the_invoice = array(
            'customer_id' => $customer_id,
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }

    // Function to perform the transacrtion using Braintree
    public function createTransaction($nonce,$customerId,$planId,$subscribed,$total,$id){

        // Pass the transaction details to Braintree and capture the result
        $result = Braintree_Transaction::sale([
            'customerId' => $customerId,
            'amount' => $total,
            'paymentMethodNonce' => $nonce,
            'orderId' => 'TM_' . Carbon::now()->timestamp . "_" . $id,
            'options' => [
                'submitForSettlement' => True
            ]
        ]);
    
        if ($result->success) {
            // Return the successful Transaction ID
            return $result->transaction->id;
        }else{

            // Create empty variable to store errors
            $errorFound = '';

            // Loop through errors and add them to the error string
            foreach ($result->errors->deepAll() as $error1) {
                $errorFound .= $error1->message . "<br />";
            }

            session(['payment_error' => $errorFound]);
            return "error";
        }
    }

    // Function to send new account message
    public function paymentConfirmed($invoice, $user, $password){
         
        Mail::send('emails.accountCreated',[
        'user' => $user,
        'password' => $password
        ], function ($message) use ($user, $password)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison - End Range Training');
            $message->subject('Account Created');
            // $message->to($user->email);
            $message->to('luke@icon-creative.com');
        });

        Mail::send('emails.adminPurchaseConfirmed',[
        'invoice' => $invoice, 
        'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'End Range Training');
            $message->subject('Purchase Confirmed');
            // $message->to('hello@tommorrison.uk');
            $message->to('luke@icon-creative.com');
        });

        return "success";
    }
}
