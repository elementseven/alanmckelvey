<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use Auth;
use Cart;
use Mail;
use Illuminate\Support\Facades\Input;
use App\Product;
use App\Invoice;
use App\User;
use Carbon\Carbon;

class PaymentController extends Controller
{
	protected $provider;
	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}
    public function expressCheckoutNewUser(Request $request){

        // Validate the form data
        $this->validate($request,[
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);
        session(['first_name' => $request->input('first_name')]);
        session(['last_name' => $request->input('last_name')]);
        session(['email' => $request->input('email')]);

        // check if payment is recurring
        $recurring = false;
        $now = Carbon::now();

        // Get the cart data
        $cart = $this->getCart($recurring, session('invoice_id')."_".$now->toW3cString());

        // send a request to paypal 
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart, $recurring);

        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            session(['payment_error' => $response['L_LONGMESSAGE0']]);
            return redirect()->to('/basket');
        }

        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
    }

    public function expressCheckout(Request $request){
        $user = Auth::user();
        $products = array();
        foreach($user->invoices as $invoice){
            foreach($invoice->products as $product){
                array_push($products, $product);
            }
        }
        foreach(Cart::content() as $item){
            $counter = 0;
            if(count($products) > 0){
                foreach($products as $p){
                    if($p->id == $item->id){
                        $counter++; 
                    }
                }
            }
            if($counter!=0){
                session(['payment_error' => 'You have already purchased at least one of the items in your basket, there is no need to pay twice. Simply login and re-download your purchase.']);
                return redirect()->to('/basket');
            }
        }
		// check if payment is recurring
		$recurring = false;
        $now = Carbon::now();
		// Get the cart data
		$cart = $this->getCart($recurring, session('invoice_id')."_".$now->toW3cString());

		// send a request to paypal 
		// paypal should respond with an array of data
		// the array should contain a link to paypal's payment system
		$response = $this->provider->setExpressCheckout($cart, $recurring);

		// if there is no link redirect back with error message
		if (!$response['paypal_link']) {
			session(['payment_error' => $response['L_LONGMESSAGE0']]);
            return redirect()->to('/basket');
		}

		// redirect to paypal
		// after payment is done paypal
		// will redirect us back to $this->expressCheckoutSuccess
		return redirect($response['paypal_link']);
    }

    public function expressCheckoutSuccess(Request $request) {

        // check if payment is recurring
        $recurring = false;
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        // initaly we paypal redirects us back with a token but doesn't provice us any additional data so we use getExpressCheckoutDetails($token) to get the payment details
        $response = $this->provider->getExpressCheckoutDetails($token);

        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
        	session(['payment_error' => "Error processing PayPal payment"]);
            return redirect()->to('/basket');
        }

        $invoice_id = session('invoice_id');

        // get cart data
        $cart = $this->getCart($recurring, $invoice_id);

        // if payment is not recurring just perform transaction on PayPal and get the payment status
        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
        // dd($payment_status);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];

        if(session('first_name')){
            $password = $this->randomPassword();
            $user = User::create([
                'first_name' => session('first_name'),
                'last_name' => session('last_name'),
                'email' => session('email'),
                'role_id' => 2,
                'password' => bcrypt($password)
            ]);
            // Log in the new user
            Auth::login($user, true);

            // Send new account and booking confirmation emails
            $email_status = $this->accountCreated($user->first_name, $user->email, $password);

            // Forget the stored session user details
            session()->forget('first_name');
            session()->forget('last_name');
            session()->forget('email');
        }

        $user = Auth::user();
        $invoice = Invoice::find($invoice_id);
        $invoice->paid = true;
        $invoice->customer_id = $PayerID;
        $invoice->transaction_id = $token;
        $invoice->card_token = "paypal";
        $invoice->user_id = $user->id;
        $invoice->save();

        if ($invoice->paid) {
        	$user=Auth::user();
            $this->orderConfirmed($user->first_name, $user->email, $invoice->transaction_id, $invoice->products, $invoice->price);
            $this->adminNewOrder($user->first_name, $user->email, $invoice->transaction_id, $invoice->products, $invoice, $invoice->price);
        	Cart::destroy();
            return redirect('/success');
        }else{
        	session(['payment_error' => "Error processing PayPal payment"]);
        	return redirect()->to('/basket');
        }
    }

    private function getCart($recurring, $invoice_id)
    {
    	$total = 0;
        if ($recurring) {
            return [
                // if payment is recurring cart needs only one item
                // with name, price and quantity
                'items' => [
                    [
                        'name' => 'Monthly Subscription ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                        'price' => 20,
                        'qty' => 1,
                    ],
                ],

                // return url is the url where PayPal returns after user confirmed the payment
                'return_url' => url('/paypal/express-checkout-success'),
                'subscription_desc' => 'Monthly Subscription ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                // every invoice id must be unique, else you'll get an error from paypal
                'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                'invoice_description' => "Order #". $invoice_id ." Invoice",
                'cancel_url' => url('/'),
                // total is calculated by multiplying price with quantity of all cart items and then adding them up
                // in this case total is 20 because price is 20 and quantity is 1
                'total' => 20, // Total price of the cart
            ];
        }

        $a = [
	        'items' => [],
	        'return_url' => url('/paypal/express-checkout-success'),
	        'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
	        'invoice_description' => "Order #" . $invoice_id . " Invoice",
	        'cancel_url' => url('/basket'),
	        'currency' => 'GBP',
	        'total' => $total,
        ];
        foreach(Cart::content() as $item){
        	$theitem = [
                'name' => $item->name,
                'price' => $item->total,
                'qty' => 1,
            ];
            array_push($a['items'], $theitem);
            $total = $total + $item->total;
    	}
    	$a['total'] = $total;
    	return $a;
    }

    // Function to create a random password
    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    // Function to send new account message
    public function accountCreated($first_name, $email, $password){

        $subject = "Account Created";
         
        Mail::send('emails.accountCreated',[
            'name' => $first_name,
            'email' => $email, 
            'password' => $password,
            'subject' => $subject
            ], function ($message) use ($subject, $email){
                $message->from('donotreply@owseymusic.com', 'Owsey');
                $message->subject($subject);
                $message->replyTo($email);
                $message->to($email);
            }
        );
        return "success";
    }

    // Function to send new account message
    public function orderConfirmed($first_name, $email, $reference, $products, $total){

        $subject = "Order Complete";
         
        Mail::send('emails.orderComplete',[
            'name' => $first_name,
            'reference' => $reference,
            'products' => $products,
            'total' => $total
            ], function ($message) use ($subject, $email){
                $message->from('donotreply@owseymusic.com', 'Owsey');
                $message->subject($subject);
                $message->replyTo($email);
                $message->to($email);
            }
        );
        return "success";
    }

    // Function to send new account message
    public function adminNewOrder($first_name, $email, $reference, $products, $invoice, $total){

        $subject = "New Order";
         
        Mail::send('emails.adminNewOrder',[
            'name' => $first_name,
            'reference' => $reference,
            'products' => $products,
            'invoice' => $invoice,
            'total' => $total
            ], function ($message) use ($subject, $email){
                $message->from('donotreply@owseymusic.com', 'Owsey');
                $message->subject($subject);
                $message->replyTo($email);
                $message->to('owseymusic@gmail.com');
            }
        );
        return "success";
    }
}