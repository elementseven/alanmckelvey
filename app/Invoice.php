<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'customer_id','transaction_id','price','paid','user_id'
    ];
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function products(){
        return $this->belongsToMany('App\Product', 'invoice_product')->withPivot('product_id');
    }
}
