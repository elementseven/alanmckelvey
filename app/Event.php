<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Event extends Model implements HasMedia
{

    use HasMediaTrait;

    protected $fillable = [
        'name','date','time','description','link','location_id'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('big')->width(1200)->sharpen(10);
        $this->addMediaConversion('mobile')->width(368)->sharpen(10);
    }

    public function location(){
        return $this->belongsTo('App\Location');
    }
}
