<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'title','slug','status','exerpt','body','category_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $slug = strtolower($post->title);
            //Make alphanumeric (removes all other characters)
            $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
            //Clean up multiple dashes or whitespaces
            $slug = preg_replace("/[\s-]+/", " ", $slug);
            //Convert whitespaces and underscore to dash
            $slug = preg_replace("/[\s_]/", "-", $slug);

            $post->slug = $slug;
        });
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('big')->width(1200)->sharpen(10);
        $this->addMediaConversion('mobile')->width(368)->sharpen(10);
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
}