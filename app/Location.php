<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'name','city','address_line_1','address_line_2','post_code','g_maps','link'
    ];

    public function events(){
    	return $this->hasMany('App\Event');
    }
}
