<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use HasMediaTrait;
    use Notifiable;

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')->width(100)->sharpen(10);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'full_name', 'first_name', 'last_name', 'email', 'password', 'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $attributes = [
        'role_id' => 2,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($user) {
            $user->full_name = $user->first_name . ' ' . $user->last_name;
        });
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function invoices(){
        return $this->hasMany('App\Invoice');
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'product_user')->withPivot('product_id');
    }
}
