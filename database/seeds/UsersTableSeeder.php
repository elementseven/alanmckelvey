<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        User::create([
	        'first_name' => "Luke",
            'last_name' => "McKelvey",
            'email' => "admin@admin.com",
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('password'),
            'role_id' => 1,
            'country' => "United Kingdom",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
	    ]);
    }
}
