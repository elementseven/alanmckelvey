<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Category::create([
	        'name' => 'News',
	        'slug' => 'news'
	    ]);
	    Category::create([
	        'name' => 'Announcements',
	        'slug' => 'announcements'
	    ]);
        Category::create([
	        'name' => 'Albums',
	        'slug' => 'albums'
	    ]);
	    Category::create([
	        'name' => 'Singles',
	        'slug' => 'singles'
	    ]);
    }
}
